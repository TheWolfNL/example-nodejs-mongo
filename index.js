const express = require('express'),
    http = require('http'),
    Mongo = require('mongodb').MongoClient;

let DB, stats, MongoConnectionString;

const App = express();
const ServerPort = process.env.PORT || 3000,
    MongoDB = process.env.MONGO_DATABASE || 'test';
    MongoConnectionString = process.env.MONGO_PORT.replace('tcp','mongodb') + '/' + MongoDB;

Mongo.connect(MongoConnectionString, function (err, db) {
    if (err) console.error(err);
    DB = db;
    DB.createCollection('stats');
    stats = DB.collection('stats');
    stats.findOne({name: "visits"}, function (err, result) {
        if (err) console.error('err', err);
        if (result === null) {
            stats.insertOne({name: "visits", value: 0}, (err, result) => {
                if (err) console.error(err);
            });
        }
    });
});

App.get('/', function(req, res, next) {
    // Update visits
    stats.update(
        { name: "visits" },
        { $inc: { value: +1 } }
    , (err, result) => {
        if (err) console.error(err);

        // Retrieve current amount of visits
        stats.findOne({name: "visits"}, (err, result) => {
            if(err) return next(err);
            res.send('You have viewed this page ' + result.value + ' times!');
        });
    });
});

http.createServer(App).listen(ServerPort, function() {
    console.log('Listening on port ' + (ServerPort));
});